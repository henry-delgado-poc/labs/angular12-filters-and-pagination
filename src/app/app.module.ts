import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactsComponent } from './contacts/contacts.component';
import { UsdToLpsPipe } from './pipes/usd-to-lps.pipe';
import { ConversionsComponent } from './conversions/conversions.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactsComponent,
    UsdToLpsPipe,
    ConversionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
