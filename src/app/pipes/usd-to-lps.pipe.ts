import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "usdToLps",
})
export class UsdToLpsPipe implements PipeTransform {
  transform(value: number, ...args: number[]): number {
    const [price] = args;
    return value * price;
  }
}
