import { RouterModule, Routes } from "@angular/router";

import { ContactsComponent } from "./contacts/contacts.component";
import { ConversionsComponent } from "./conversions/conversions.component";
import { NgModule } from "@angular/core";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/contacts",
  },
  {
    path: "contacts",
    component: ContactsComponent,
  },
  {
    path: "conversions",
    component: ConversionsComponent,
  },
  {
    path: "**",
    redirectTo: "/",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
